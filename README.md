# RFH = Request for Hacks

Like RFC ("Request for Comments") or RFS ("Request for Startups") but by developers, for developers.

Every developer should maintain a list of desirable improvements in our tools or new software projects that would make our lives easier, both as developers and as users. Maintaining them as issues on Github/Gitlab/Codeberg allows search, labeling, resolution and discussion.

### [Here are my requests for hacks](https://codeberg.org/nilesh/random-ideas/issues)

[Twitter Thread](https://twitter.com/nileshtrivedi/status/1776125686543159723)

<img width="553" alt="image" src="https://github.com/nileshtrivedi/gupshup/assets/19304/e78775db-8fa2-4203-acea-387f5e9845e9">